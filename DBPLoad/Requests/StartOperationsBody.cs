﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace DBPLoad.Requests
{
	public class StartOperationsBody : IRequestBody
	{
		private StringContent _body;

		public StartOperationsBody(StartOperationOption startOperationOption, string value)
		{
			JObject startJson = new JObject();
			switch (startOperationOption)
			{
				case StartOperationOption.ByService:
					startJson.Add("serviceId", value);
					break;
				case StartOperationOption.ByTemplate:
					startJson.Add("templateId", value);
					break;
				case StartOperationOption.ByEventId:
					startJson.Add("sourceEventId", value);
					break;
				case StartOperationOption.ByPaymentId:
					startJson.Add("paymentId", value);
					break;
				default: break;
			}
			_body = new StringContent(JsonConvert.SerializeObject(startJson), Encoding.UTF8, "application/json");
		}
		public HttpContent CreateBodyMessage()
		{
			return _body;
		}

		public StringContent CreateStartScenarioByServiceBody(string serviceId)
		{
			JObject startJson = new JObject();
			startJson.Add("serviceId", serviceId);
			_body = new StringContent(JsonConvert.SerializeObject(startJson), Encoding.UTF8, "application/json");
			return _body;
		}
	}
}
