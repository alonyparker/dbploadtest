﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace DBPLoad.Requests
{
	//добавить в конструктор httpclient
	public class Get : IRequest
	{
		
		private Client _client;
		public Get(Client client)
		{
			_client = client;
		}
		public async Task<HttpResponseMessage> Send(string url)
		{
			HttpResponseMessage response = null;
			response = await _client.HttpClient.GetAsync(url);

			return response;
		}

	}
}
