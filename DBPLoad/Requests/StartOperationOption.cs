﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBPLoad.Requests
{
	public enum StartOperationOption
	{
		ByService,
		ByTemplate,
		ByPaymentId,
		ByEventId
	}
}
