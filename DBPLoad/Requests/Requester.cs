﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
namespace DBPLoad.Requests
{
	public class Requester
	{
		// у всех запросов должен быть один и тот же Client чтобы обновлялись куки
		//прокинуть 

		public Get Get;
		public Post Post;

		public Client HttpClient;
		public Client AMClient;
		public ResponseProcessor ResponseProcessor;


		public Requester(string DbpApiPath)
		{
			HttpClient = new Client();
			HttpClient.InitHttpClient(DbpApiPath);
			Get = new Get(HttpClient);
			Post = new Post(HttpClient);
			ResponseProcessor = new ResponseProcessor();

		}

		public void AddSetCookieHeaders(HttpResponseMessage response)
		{
			var headers = this.ResponseProcessor.GetSetCookietHeaders(response);
			if (headers != null)
				this.HttpClient.AddCookieHeader(headers);
		}

		public string ChangeUrlIfNeed(HttpResponseMessage response)
		{
			var redirectUrl = this.ResponseProcessor.GetRedirectUrl(response);
			/*if (!string.IsNullOrEmpty(redirectUrl))
				this.HttpClient.AddCustomHeader("Referer", response.RequestMessage.RequestUri.ToString());*/
			return redirectUrl;
		}
	}
}
