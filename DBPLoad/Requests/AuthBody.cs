﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http.Formatting;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace DBPLoad.Requests
{
	public class AuthBody : IRequestBody
	{
		private StringContent _authBody;
		public AuthBody()
		{
			_authBody = new StringContent("");
		}
		public HttpContent CreateBodyMessage()
		{
			return _authBody;
		}

		public StringContent CreateAuthForm(HttpResponseMessage authForm, string user, string password)
		{
			var json = authForm.Content.ReadAsAsync<JObject>().Result;
			JArray errors = (JArray)json["errors"];
			JArray calback = (JArray)json["callbacks"];
			CheckError(json, authForm);
			FillFormParam(ref calback, "username", user);
			FillFormParam(ref calback, "password", password);
			//FillFormParam(ref calback, "setprotectcode", "false");
			FillFormParam(ref calback, "confirmation", "0");

			string jsonWithValue = JsonConvert.SerializeObject(json);
			_authBody = new StringContent(jsonWithValue, Encoding.UTF8, "application/json");
			return _authBody;
		}

		public StringContent CreateOtpForm(HttpResponseMessage otpForm, string otp)
		{
			var json = otpForm.Content.ReadAsAsync<JObject>().Result;
			JArray calback = (JArray)json["callbacks"];

			CheckError(json, otpForm);
			FillFormParam(ref calback, "otp", otp);
			FillFormParam(ref calback, "confirmation", "0");
			

			string jsonWithValue = JsonConvert.SerializeObject(json);
			_authBody = new StringContent(jsonWithValue, Encoding.UTF8, "application/json");
			return _authBody;
		}

		public StringContent CreateChooseNotificationForm(HttpResponseMessage chooseNotificationForm)
		{
			var json = chooseNotificationForm.Content.ReadAsAsync<JObject>().Result;
			JArray calback = (JArray)json["callbacks"];

			CheckError(json, chooseNotificationForm);
			FillFormParam(ref calback, "channels", "sms");
			FillFormParam(ref calback, "confirmation", "0");

			string jsonWithValue = JsonConvert.SerializeObject(json);
			_authBody = new StringContent(jsonWithValue, Encoding.UTF8, "application/json");
			return _authBody;
		}


		public void FillFormParam(ref JArray callbacks, string propertyName, string propertyValue)
		{
			if (callbacks.Children<JObject>().Any(x => x.Property("name").Value.ToString().Contains(propertyName)))
			{
				callbacks.Children<JObject>().FirstOrDefault
					(x => x.Property("name").Value.ToString().Contains(propertyName)).Property("name")
					.AddAfterSelf(new JProperty("value", propertyValue));
			}
			else
			{
				Assert.Fail($"Отсутствует поле {propertyName} в json : \n {callbacks.ToString()}");
			}

			
		}

		private void CheckError(JObject amResponse, HttpResponseMessage response)
		{
			string amFails = "";
			
			JArray errors = (JArray)amResponse["errors"];
			foreach (JObject entry in errors)
			{
				amFails = amFails + " Code = " + entry.Property("code").Value.ToString() + " Value = " + entry.Property("value").Value.ToString() + "\n";
			}

			if (!string.IsNullOrEmpty(amFails))
				Assert.Fail("URI = " + response.RequestMessage.RequestUri.ToString() + amFails );
		}
	}
}
