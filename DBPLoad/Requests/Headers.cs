﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBPLoad.Requests
{
	public static class Headers
	{
		public static (string Name, string Value) UserAgent = ("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");
		public static (string Name, string Value) ContentTypeForm = ("Content-Type", "application/x-www-form-urlencoded");
		public static (string Name, string Value) AcceptXml = ("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8 ");
		public static (string Name, string Value) AcceptXmlJson = ("Accept", "text/html, application/xhtml+xml, application/xml; q=0.9, image/webp, image/apng, */*; q=0.8, application/json, text/plain ");
		public static (string Name, string Value) AcceptLanguage = ("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
		public static (string Name, string Value) XRequestedWith = ("X-Requested-With", "XMLHttpRequest");
		public static (string Name, string Value) DNT = ("DNT", "1");
		public static (string Name, string Value) ConnectionKeepAlive = ("Connection", "keep-alive");
		public static string CorrelationIdCookieName = ".AspNetCore.Correlation.oidc";
		public static string SetCookieHeader = "Set-Cookie";
		public static (string Name, string Value) ApiVersion = ("X-Api-Version", "1.0");
	}
}
