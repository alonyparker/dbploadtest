﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace DBPLoad.Requests
{
	public class ResponseProcessor 
	{
		public string GetRedirectUrl(HttpResponseMessage response)
		{
			//string redirectUrl = "";
			string redirectUrl=null;
			if (response.Headers.Any(x => x.Key.Contains("Location")))
			{
				//ElementEt(0) убрать
				redirectUrl = response.Headers.First(x => x.Key.Contains("Location")).Value.ElementAt(0).ToString();
			}
			else
			{
	
				if (response.Headers.Any(x => x.Key.Contains("X-Responded-JSON")))
				{
					dynamic jsonResp = JsonConvert.DeserializeObject(response.Headers.First(x => x.Key.Contains("X-Responded-JSON")).Value.First<string>());
					redirectUrl = ((JObject)jsonResp).SelectToken("headers.location").ToString();;
					if (!redirectUrl.StartsWith("http"))
					{
						try
						{
							JToken locationValue;
							var json = response.Content.ReadAsAsync<JObject>().Result;
							if (json != null && json.HasValues && json.TryGetValue("location", out locationValue))
							{
								string location = json.Property("location").Value.ToString();
								return location;

							}
						}
						catch (Exception e)
						{
							Assert.Fail(e.ToString());
						}

					}
				}
			}

			return redirectUrl;

		}

		public IEnumerable<string> GetSetCookietHeaders(HttpResponseMessage response)
		{
			IEnumerable<string> headers = null ;
			if (response.Headers.Any(x => x.Key.Contains(Headers.SetCookieHeader)))
			{
				var setCookieHeaders = response.Headers.Where(x => x.Key.Contains(Headers.SetCookieHeader));
				headers = setCookieHeaders.First().Value;
				foreach (var header in setCookieHeaders)
					headers.Concat(header.Value);
			}
			return headers;

		}


		//!!!!!! переписать чтобы не было эксепшенов. + объединить GetCorrelationOidcCookie и GetCabinetSession
		public string GetCorrelationOidcCookie(HttpResponseMessage response)
		{
			string cookie = "";
			try
			{
				var headers = this.GetSetCookietHeaders(response);
				var correlationId = headers.First(x => x.Contains(Headers.CorrelationIdCookieName));
				var setCookieArray = correlationId.Split(';');
				cookie = setCookieArray.First(x => x.Contains(Headers.CorrelationIdCookieName));
			}
			catch (Exception e)

			{
				Assert.Fail(e.ToString());
			}
			return cookie;
		}

		public string GetCabinetSession(HttpResponseMessage response)
		{
			string cookie = null;
			try
			{
				var headers = this.GetSetCookietHeaders(response);
				var correlationId = headers.First(x => x.Contains("cabinet_session"));
				var setCookieArray = correlationId.Split(';');
				cookie = setCookieArray.First(x => x.Contains("cabinet_session"));
			}
			catch (Exception e)
			{
				//Assert.Fail(e.ToString());
			}
			return cookie;
		}


	}
}
