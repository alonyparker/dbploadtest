﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DBPLoad.Requests
{
	public class Post : IRequest
	{
		public HttpContent _body = new StringContent("");
		private Client _client;
		public Post(Client client)
		{
			_client = client;
		}
		public async Task<HttpResponseMessage> Send(string url)
		{
			HttpResponseMessage response = null;
			response = await _client.HttpClient.PostAsync(url, _body);
			_body = new StringContent("");
			return response;
		}

		public void AddBody(IRequestBody requestBody)
		{
			_body = requestBody.CreateBodyMessage();
		}

	}
}
