﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DBPLoad.Requests
{
	public class Client 
	{
		private HttpClient _httpClient;
		private HttpClientHandler clientHandler;
		public Client()
		{
			clientHandler = new HttpClientHandler()
			{
				UseCookies = false,
				AllowAutoRedirect = false
			};
		}

		public HttpClient HttpClient
		{
			get
			{
				return _httpClient;
			}
		}

		//сначала удалять хэдеры с куками, потом добавлять
		public HttpClient AddCookieHeader(IEnumerable<string> headers)
		{
			_httpClient.DefaultRequestHeaders.Remove("Cookie");
			string cookies = "";
			foreach (var header in headers)
			{
				var headersArray = header.Split(';');
				//ОБЯЗАТЕЛЬНО ИСПРАВИТЬ!!!!!!!!!!
				if (!cookies.Contains(headersArray[0]) && !headersArray[0].Contains("=."))
				{
					//cookies = cookies + "; " + headersArray[0];
					cookies = cookies + headersArray[0] + ";" ;
				}
			}

			//исправить тоже 
			if (!_httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Cookie", cookies))
				Assert.Fail();
			return this._httpClient;
		}
		public HttpClient AddCookieHeader(string headers)
		{
			_httpClient.DefaultRequestHeaders.Remove("Cookie");
			
			//исправить тоже 
			if (!_httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Cookie", headers))
				Assert.Fail();
			return this._httpClient;
		}


		public HttpClient InitHttpClient(string baseUri)
		{

			_httpClient = new HttpClient(clientHandler);
			var baseUrl = new Uri(baseUri);

			_httpClient.BaseAddress = baseUrl;
			
			_httpClient = new HttpClient(clientHandler);
			_httpClient.DefaultRequestHeaders.TryAddWithoutValidation(Headers.ContentTypeForm.Name, Headers.ContentTypeForm.Value); 
			_httpClient.DefaultRequestHeaders.TryAddWithoutValidation(Headers.XRequestedWith.Name, Headers.XRequestedWith.Value);
			_httpClient.DefaultRequestHeaders.TryAddWithoutValidation(Headers.AcceptLanguage.Name, Headers.AcceptLanguage.Value);
			_httpClient.DefaultRequestHeaders.TryAddWithoutValidation(Headers.UserAgent.Name, Headers.UserAgent.Value);
			_httpClient.DefaultRequestHeaders.TryAddWithoutValidation(Headers.DNT.Name, Headers.DNT.Value);
			_httpClient.DefaultRequestHeaders.TryAddWithoutValidation(Headers.ConnectionKeepAlive.Name, Headers.ConnectionKeepAlive.Value);
			_httpClient.DefaultRequestHeaders.TryAddWithoutValidation(Headers.AcceptXmlJson.Name, Headers.AcceptXmlJson.Value);
			_httpClient.DefaultRequestHeaders.TryAddWithoutValidation(Headers.ApiVersion.Name, Headers.ApiVersion.Value);

			return _httpClient;
		}
		public bool AddCustomHeader(string header, string value)
		{
			var result = _httpClient.DefaultRequestHeaders.TryAddWithoutValidation(header, value);
			return result;
		}
	}
}
