﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Resources;
using System.Reflection;
using DBPLoad.Utils;

namespace DBPLoad.TestEnvironment
{
	public class SpLetoSb3 : ITestStand
	{
		private ResourceManager rm = new ResourceManager(typeof(sp_leto_sb3));

		public static SpLetoSb3 Create()
		{
			return new SpLetoSb3();
		}
		public TestData InitTestData()
		{
			TestData testData = new TestData()
			{

				ConnectionStringAmDb = rm.GetString("ConnectionStringAmDb"),
				DefaultPassword = rm.GetString("DefaultPassword"),
				Usernames = new List<string>(),
				otpStub = rm.GetString("otpStub"),
				DBPApiPath = rm.GetString("DbpApiPath")

			};
			testData.Usernames = InitUserLogins(testData.ConnectionStringAmDb);
			return testData;
		}

		private List<string> InitUserLogins(string connectionString)
		{
			List<string> usernames = new List<string>();
			using (var db = new UnitOfWork(connectionString))
			{
				foreach (var user in db.Users.GetAll().Where(x => x.Login.Contains("testuser1")))
				{
					usernames.Add(user.Login);
				};
			}

			return usernames;
		}
	}
}
