﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBPLoad.TestEnvironment
{
	public interface ITestStand
	{
		TestData InitTestData();
		
	}
}
