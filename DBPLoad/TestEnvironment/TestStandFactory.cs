﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using  Microsoft.VisualStudio.TestTools.UnitTesting;
namespace DBPLoad.TestEnvironment
{
	public class TestStandFactory
	{
		private static Dictionary<string, Func<ITestStand>> InstanceCreateCache = new Dictionary<string, Func<ITestStand>>();

		public ITestStand InitTestEnviroment(string testContext)
		{
			try
			{
				if (!InstanceCreateCache.ContainsKey(testContext))
				{
					Type stand = TypeDelegator.GetType("DBPLoad.TestEnvironment." + testContext);
					MethodInfo mi = stand.GetMethod("Create");
					var metods = stand.GetMethods();
					var createInstanceDelegate = (Func<ITestStand>)Delegate.CreateDelegate(typeof(Func<ITestStand>), mi);
					InstanceCreateCache.Add(testContext, createInstanceDelegate);
				}

				return InstanceCreateCache[testContext].Invoke();
			}
			catch (Exception e)
			{
				Assert.Fail(e.ToString());
			}
			return null;
		}
	}
}
