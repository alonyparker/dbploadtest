﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DBPLoad.Requests;
using DBPLoad.Utils;
namespace DBPLoad.Tests
{
	[TestClass]
	public class LoginTests : CommonWraps
	{

		[TestInitialize]
		public void RefreshHttpClient()
		{

			this.userLogin = testData.Usernames.ElementAt(rnd.Next(0, testData.Usernames.Count()));
			Requester = new Requester(testData.DBPApiPath);
		}

		[TestMethod]
		public async Task Login_Test()
		{
			string cabinetSession = await this.Login(this.userLogin, "Qwe1234$");

			if (string.IsNullOrEmpty(cabinetSession))
				Assert.Fail("Не получена сессия");
		}
	}
}
