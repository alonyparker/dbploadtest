﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net.Http.Headers;
using DBPLoad.Requests;
using DBPLoad.TestEnvironment;
using DBPLoad.Utils;

namespace DBPLoad.Tests
{
	[TestClass]
	public class TestBase
	{
		public Requester Requester;
		protected string userLogin;
		protected Random rnd = new Random();
		protected static TestData testData;
		private static TestStandFactory testStandFactory = new TestStandFactory();

		[AssemblyInitialize]
		public static void InitAllTests(TestContext testContext)
		{
			ITestStand testStand;
			if (testContext.Properties.Contains("TestStand"))
			{
				testStand = testStandFactory.InitTestEnviroment(testContext.Properties["TestStand"].ToString());
			}
			else
			{
				//default stand - пока spletosb3
				testStand = testStandFactory.InitTestEnviroment("SpLetoSb3");
				
			}
			testData = InitTestData(testStand);

		}

		private static TestData InitTestData(ITestStand testStand)
		{
			return testStand.InitTestData();
		}

	}
}
