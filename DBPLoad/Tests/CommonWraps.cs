﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBPLoad.Requests;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace DBPLoad.Tests
{
	public class CommonWraps : TestBase
	{
		private string url = "";
		protected async Task<string> Login(string user, string password)
		{
			 url = testData.DBPApiPath + "user/login";

			//отправить get user/login
			var response = await Requester.Get.Send(url);

			var correlationIdCookie = Requester.ResponseProcessor.GetCorrelationOidcCookie(response);
			url = Requester.ChangeUrlIfNeed(response)??url;
			response = await Requester.Get.Send(url);

			//получить редирект url на ам
			url = Requester.ChangeUrlIfNeed(response) ?? url;
			//добавить хедеры к следующему запросу - пост
			Requester.AddSetCookieHeaders(response);
			response = await Requester.Get.Send(url);


			Requester.AddSetCookieHeaders(response);
			//создаем тело запроса
			var body = new AuthBody();
			body.CreateAuthForm(response, user, password);

			//добавить тело
			Requester.Post.AddBody(body);
			response = await Requester.Post.Send(url);

			Requester.AddSetCookieHeaders(response);
			body.CreateOtpForm(response, testData.otpStub);
			Requester.Post.AddBody(body);
			response = await Requester.Post.Send(url);

			if (IsFirstLogin(response))
			{
				Requester.AddSetCookieHeaders(response);
				body.CreateChooseNotificationForm(response);
				Requester.Post.AddBody(body);
				response = await Requester.Post.Send(url);
			}

			//редирект на oidc
			url = Requester.ChangeUrlIfNeed(response) ?? url;
			Requester.AddSetCookieHeaders(response);
			response = await Requester.Get.Send(url);

			url = Requester.ChangeUrlIfNeed(response) ?? url;
			Requester.HttpClient.AddCustomHeader("Cookie", correlationIdCookie);
			response = await Requester.Get.Send(url);

			string cabinetSession = Requester.ResponseProcessor.GetCabinetSession(response);
			url = Requester.ChangeUrlIfNeed(response) ?? url;
			Requester.AddSetCookieHeaders(response);
			response = await Requester.Get.Send(url);
			
			//пока так, проблема с платформой при первом входе
			cabinetSession = Requester.ResponseProcessor.GetCabinetSession(response) ?? cabinetSession;

			return await Task<string>.Factory.StartNew(() => cabinetSession);

		}

		private bool IsFirstLogin(HttpResponseMessage response)
		{
			var headers = Requester.ResponseProcessor.GetSetCookietHeaders(response);
			if (headers != null)
			{
				if (headers.Any(x => x.Contains("am_session")))
					return false;
				else return true;
			}
			else return true;
		}

		//
		protected string GetFirstCardContractId()
		{
			//заглушка
			return "CARD403317QLCPYD1111";
		}
	}
}
