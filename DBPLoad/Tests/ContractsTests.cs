﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DBPLoad.Requests;

namespace DBPLoad.Tests
{
	[TestClass]
	public class ContractsTests : CommonWraps
	{
		[TestInitialize]
		public void RefreshHttpClient()
		{
			this.userLogin = testData.Usernames.ElementAt(rnd.Next(0, testData.Usernames.Count()));
			Requester = new Requester(testData.DBPApiPath);
		}

		[TestMethod]
		public async Task GetContracts_Test()
		{
			string cabinetSession = await this.Login(this.userLogin, "Qwe1234$");

			if (string.IsNullOrEmpty(cabinetSession))
				Assert.Fail("Не получена сессия");
			Requester.HttpClient.AddCookieHeader(cabinetSession);
			var response = await Requester.Get.Send(testData.DBPApiPath + "banking/contracts");
			Assert.IsTrue(response.IsSuccessStatusCode, $"код ответа: {response.StatusCode.ToString()}");
		}

		[TestMethod]
		public async Task StartOperationByContract_Test()
		{
			string cabinetSession = await this.Login(this.userLogin, "Qwe1234$");

			if (string.IsNullOrEmpty(cabinetSession))
				Assert.Fail("Не получена сессия");
			Requester.HttpClient.AddCookieHeader(cabinetSession);

			var body = new StartOperationsBody(StartOperationOption.ByService, "9");
			
			Requester.Post.AddBody(body);
			var response = await Requester.Post.Send(testData.DBPApiPath + "banking/contracts/" + this.GetFirstCardContractId() + "/operations");

			Assert.IsTrue(response.IsSuccessStatusCode, $"код ответа: {response.StatusCode.ToString()}, Сообщение: {response.Content.ReadAsStringAsync().Result}");
		}
	}
}
