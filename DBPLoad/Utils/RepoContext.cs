﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace DBPLoad.Utils
{
	public class RepoContext : DbContext
	{
		public RepoContext(string connectionString) : base (connectionString)
		{
		}
		static RepoContext()
		{
			Database.SetInitializer<RepoContext>(null);
		}

		public DbSet<User> Users { get; set; }

	}
}
