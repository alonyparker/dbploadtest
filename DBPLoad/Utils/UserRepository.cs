﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;

namespace DBPLoad.Utils
{
	public class UserRepository : IRepository<User>
	{
		private RepoContext db;

		public UserRepository(RepoContext context)
		{
			this.db = context;
		}

		public User Get(int id)
		{
			var user = db.Users.Find(id);

			return user;
		}

		public IEnumerable<User> GetAll()
		{
			return db.Users;
		}
	
	}
}
