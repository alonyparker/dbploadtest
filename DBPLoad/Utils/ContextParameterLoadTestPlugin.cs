﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.LoadTesting;

namespace DBPLoad.Utils
{
	public class ContextParameterLoadTestPlugin : ILoadTestPlugin
	{
		LoadTest LoadTest;
		public void Initialize(LoadTest LoadTest)
		{
			this.LoadTest = LoadTest;
			this.LoadTest.TestStarting += new EventHandler<TestStartingEventArgs>(TestStarting);
		}

		void TestStarting(object sender, TestStartingEventArgs e)
		{
			foreach (string key in LoadTest.Context.Keys)
			{
				e.TestContextProperties.Add(key, LoadTest.Context[key]);
			}

		}
	}
}
