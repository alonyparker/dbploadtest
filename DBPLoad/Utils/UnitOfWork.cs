﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBPLoad.Utils
{
	public class UnitOfWork : IDisposable
	{
		private RepoContext db;
		private UserRepository userRepository;
		private bool disposed = false;

		//потом переделать в фабрику, чтобы подключаться к разным базам
		public UnitOfWork(string connectionString)
		{
			db = new RepoContext(connectionString);
		}
		public UserRepository Users
		{
			get
			{
				if (userRepository == null)
					userRepository = new UserRepository(db);
				return userRepository;
			}
		}

		public void Save()
		{
			db.SaveChanges();
		}

		public virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
					db.Dispose();
				this.disposed = true;
			}
		}
		public void Dispose()
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}
		}
}
