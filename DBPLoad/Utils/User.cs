﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBPLoad.Utils
{
	[Table("User")]
	public class User
	{
		public long RowId;

		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Key]
		public string IdentityId { get; set; }
		public string Login { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string DisplayName { get; set; }
		public string Email { get; set; } 
		public string MobilePhoneNumber { get; set; }
		public bool Enabled { get; set; }
		public string Description { get; set; }
		public string ExternalId { get; set; }
		public Byte[] Version { get; set; }

	}
}
